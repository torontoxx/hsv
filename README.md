# README #

Article ["Short Nucleotide Sequences in Herpesviral Genomes Identical to the Human DNA"](http://dx.doi.org/10.1016/j.jtbi.2015.02.019) by F.Filatov and A.Shargunov, Journal of Theoretical Biology, 2015, describes the search and genomic allocation of such sequences in the human herpesviral genomes. Authors consider these sequences as putative targets for human RNAi molecules and their allocation as hypothetical targetome map of the human herpesviruses. The computer algorithm for the study (the source code) is now available in the Bitbucket VCS.

## The short description of the applications:

1. **CreateSummaryTable** *(C++ Win32 application, MS Visual Studio project)*. Converts the raw data for herpesvirus in our own format, more convenient for later use.
1. **FastaToEmbl** *(Python 3.x application)*. Converts the raw data for the human DNA (chromosomes) from FASTA into EMBL format for later use.
1. **VirusVsHuman** *(C++ Win32 application, MS Visual Studio project)*. Searches strict 20bp homology of herpesvirus genes in the human genome.
1. **HitsStatistic** *(C++ Win32 application, MS Visual Studio project)*. Combines homologies found in the previous step into the hits and counts their statistics for herpesvirus genes, as well as for human chromosomes.
          

## Applications above organize the next workflow:

**1)** Preparation the raw data for herpesviruses from the next NCBI FTP sites:
[HHV1](ftp://ftp.ncbi.nih.gov/genomes/Viruses/Human_herpesvirus_1_uid15217/), 
[HHV2](ftp://ftp.ncbi.nih.gov/genomes/Viruses/Human_herpesvirus_2_uid15218/), 
[HHV3](ftp://ftp.ncbi.nih.gov/genomes/Viruses/Human_herpesvirus_3_uid15198/), 
[HHV4](ftp://ftp.ncbi.nih.gov/genomes/Viruses/Human_herpesvirus_4_uid14413/), 
[HHV4-2](ftp://ftp.ncbi.nih.gov/genomes/Viruses/Human_herpesvirus_4_type_2_uid20959/), 
[HHV5](ftp://ftp.ncbi.nih.gov/genomes/Viruses/Human_herpesvirus_5_uid14559/), 
[HHV7](ftp://ftp.ncbi.nih.gov/genomes/Viruses/Human_herpesvirus_7_uid14625/), 
[HHV8](ftp://ftp.ncbi.nih.gov/genomes/Viruses/Human_herpesvirus_8_uid14158/). 
Download files with extensions `*.ptt`, `*.faa`, `*.ffn`.

**2)** The application **CreateSummaryTable** converts the data downloaded on the previous step into our own format. To run CreateSummaryTable application open console and type the following command:

   ```CreateSummaryTable.exe -in <src path> -out <dest path> -t <virus type>```

*where:* 

```<src path>``` *argument - the directory with raw data for herpesvirus downloaded on the previous step (*.ptt, *.faa, *.ffn);*

```<dest path>``` - *the directory with the obtained result - the data for herpesvirus in our own format;*

```<virus type>``` - *the herpesvirus type, can take values: 1,2,3...8.*

**3)** Preparation the raw data for the human DNA (chromosomes) from the [NCBI FTP site](ftp://ftp.ncbi.nih.gov/genomes/H_sapiens/ARCHIVE/ANNOTATION_RELEASE.104/Assembled_chromosomes/seq/).
Download and unpack these data in FASTA format: `hs_ref_GRCh37.p10_chr*.fa.gz`.

**4)** The application **FastaToEmbl** converts the data from FASTA downloaded on the previous step into EMBL format. To run FastaToEmbl application open console and type the following command:

   ```python fasta_to_embl.py -i <src path> -o <dest path>```
 
*where:*

```<src path>``` *argument - the directory with the data for the human DNA downloaded on the previous step raw (hs_ref_GRCh37.p10_chr\*.fa);*

```<dest path>``` - *the directory with the conversion result - the data of human hromosomes in EMBL format.*

**5)** To run search strict 20bp homology of herpesvirus genes in the human genome (25 chromosomes), open console and type the following command:

   ```VirusVsHuman.exe -vp <src herpesvirus path> -cp <src human genome path> -vt <virus type> -c1 1 -c2 25```

*where:*

```<src herpesvirus path>``` *argument - the directory with data for herpesvirus obtained in the step 2;*

```<src human genome path>``` - *the directory with the data for human genome obtained in the step 4;*

```<virus type>``` - *the herpesvirus type, can take values: 1,2,3...8.*

As the result at the **<src herpesvirus path>** directory will be created **"search_result"** folder with the results of search - homologies found in the human genome.

**6)** At the final stage we should combines 20bp homologies found in the previous step into the hits and counts their statistics for herpesvirus genes, as well as for human chromosomes. Open console and type the following command:

   ```HitsStatistic.exe -in <src herpesvirus path> -t <virus type>```

*where:*

```<src herpesvirus path>``` *argument - the directory with data for herpesvirus obtained in the step 2;*

```<virus type>``` - *the herpesvirus type, can take values: 1,2,3...8.*

As the result at the **"search_result"** directory with the results of search obtained on the previous step, will be created **"stat"** folder with the statistic for herpesvirus genes (**"genes"** folder) and for human genome (**"chr"** folder) in CSV format. 

