#pragma once

#include "resource.h"

bool ProcessVirus( STR virus_name, STR src_folder, long target_len, STR src_search_folder, STR res_stat_folder );
bool CalcHitsInGenesStatistic( STR virus_name, STR res_folder, CHitsLoader& hitmgr );
bool CalcHitsInChrStatistic( STR virus_name, bool use_percent, STR res_folder, CHitsLoader& hitmgr );

STR GetStr( double val, int prec );

STR HitsInGenesStatFolder( STR src )   { return src + "genes\\"; };
STR HitsInChrStatFolder( STR src )     { return src + "chr\\"; };


TCHAR* getCmdOption(TCHAR** begin, TCHAR** end, const STR& option);
bool   cmdOptionExists(TCHAR** begin, TCHAR** end, const STR& option);
STR    checkPath( STR src );
