#pragma once

struct CGene
{
	CGene( STR pid, STR name, STR synonym, STR product, bool isstraight, long pos1, long pos2, long len, STR& prot, STR& nukl );

	STR  m_PID;
	STR  m_name;
	STR  m_synonym;
	STR  m_product;
	bool m_isstraight;
	long m_pos1;
	long m_pos2;
	long m_len;
	STR  m_prot_seq;
	STR  m_nukl_seq;
};


class CVirusDataLoader
{
public:
	CVirusDataLoader(void);
	~CVirusDataLoader(void);
	void Clear();

	bool Load( STR src_filename );

	long   GetGeneListSize()   { return (long)m_gene_list.size(); };
	CGene* GetGene( long ind ) { return m_gene_list[ind];         };
	CGene* FindGeneByPID( STR pid );

private:

	std::vector<CGene*> m_gene_list;
};
