/*
This program was developed as part of work on article 
"Short Nucleotide Sequences in Herpesviral Genomes Identical to the Human DNA", Journal of Theoretical Biology (2015).
Authors: F.Filatov, A.A.Shargunov.
This program combines homologies found in the previous step into the hits and counts their statistics for herpesvirus genes, as well as for human chromosomes.
For additional information see: https://bitbucket.org/torontoxx/hsv
*/

#include "stdafx.h"

#include "Global.h"

#include "VirusDataLoader.h"

#include "HitsLoader.h"

#include "HitsStatistic.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		_tprintf(_T("Fatal Error: MFC initialization failed\n"));
		return 1;
	}

	long target_len = 20; // ����� ������� [bp]

	if (argc < 3) 
	{
		puts("Usage: -in <indir> -t <virus type>"); 
        getchar();
        return 0;
	}

	STR src_folder = getCmdOption(argv, argv + argc, "-in");   
	STR virus_type = getCmdOption(argv, argv + argc, "-t");

	src_folder = checkPath( src_folder );


	VECTOR_STR virus_list;

	virus_list.push_back( virus_type );

	STR add_folder = "";

	//--------------------------------------

	char buf[10];
	_ltoa_s( target_len, buf, 10 );

	STR src_search_folder = src_folder + "search_result\\" + buf + "\\" + add_folder; // ����� � ������������ ������

	STR res_stat_folder = src_search_folder + "stat\\";
	
	_mkdir( res_stat_folder.c_str() );

	_mkdir( HitsInGenesStatFolder( res_stat_folder ).c_str() );
	_mkdir( HitsInChrStatFolder( res_stat_folder ).c_str() );

	//--------------------------------------

	for (size_t v=0; v<virus_list.size(); ++v )
	{
		STR virus_name = virus_list[v];

		printf_s( "\nProcessing %s...\n", virus_name.c_str() );

		if (ProcessVirus( virus_name, src_folder, target_len, src_search_folder, res_stat_folder )) printf_s( "OK.\n" );
		else                                                                                        printf_s( "FAIL!.\n" );
	}

	//--------------------------------------

	printf_s( "\nComplete\n" );
	getchar();

	// clear all:

	virus_list.clear();

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////

bool ProcessVirus( STR virus_name, STR src_folder, long target_len, STR src_search_folder, STR res_stat_folder )
{
	printf_s( "   loading data..." );

	CVirusDataLoader datamgr;

	STR src_filename = src_folder + virus_name + ".gff";

	if (!datamgr.Load( src_filename )) return false;

	printf_s( "OK(%ld)\n", datamgr.GetGeneListSize() );
	
	printf_s( "   loading hits..." );

	CHitsLoader hitmgr;

	src_filename = src_search_folder + virus_name + ".gff";

	if (!hitmgr.Load( src_filename, datamgr, target_len )) return false;

	printf_s( "OK(%ld)\n", hitmgr.GetGeneListSize() );
	
	printf_s( "   create statistic..." );

	CalcHitsInGenesStatistic( virus_name, HitsInGenesStatFolder(res_stat_folder), hitmgr );

	CalcHitsInChrStatistic( virus_name, false, HitsInChrStatFolder(res_stat_folder), hitmgr );

	printf_s( "OK\n" );

	// clear all:

	datamgr.Clear();
	hitmgr.Clear();

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

bool CalcHitsInGenesStatistic( STR virus_name, STR res_folder, CHitsLoader& hitmgr )
{
	STR res_filename = res_folder + virus_name + ".txt";
	
	FILE* resfile = CreateFile( res_filename.c_str());
	if (!resfile) return false;

	fprintf_s( resfile, "%s	%s	%s	%s	%s	%s", 
		                "gene name", 
						"gene len[aa]", 
						"hits count", 
						"hits count/gene len", 
						"hits len[bp]", 
						"hits len/gene len[%]" );

	fputs( "\n\n", resfile );

	for (long i=0; i<hitmgr.GetGeneListSize(); ++i )
	{
		CHitGene* p_gene = hitmgr.GetGene(i);

//		p_gene->ShowHitsInGene();

		STR gene_name        = p_gene->m_pParent->m_name;
	
		long gene_len        = p_gene->GetGeneLengthAA();  // [aa]
		
		long hits_cnt        = p_gene->GetHitsInGeneCount();

		double hits_density  = p_gene->GetHitsInGeneDensity();

		long hits_len        = p_gene->GetHitsInGeneLength(); // [bp]

		double hits_len_perc = p_gene->GetHitsInGeneLengthPercent();

		fprintf_s( resfile, "%s	%ld	%ld	%s	%ld	%s\n", 
			                gene_name.c_str(), 
							gene_len, 
							hits_cnt, 
							GetStr( hits_density, 6 ).c_str(), 
							hits_len, 
							GetStr( hits_len_perc, 1 ).c_str() );
	}

	fclose(resfile);

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

bool CalcHitsInChrStatistic( STR virus_name, bool use_percent, STR res_folder, CHitsLoader& hitmgr )
{
	STR res_filename = res_folder + virus_name + ".txt";
	
	FILE* resfile = CreateFile( res_filename.c_str());
	if (!resfile) return false;

	fprintf_s( resfile, "%s	%s	%s	%s", 
		                "gene name", 
						"gene len[aa]", 
						"hits count", 
						"hits count/gene len" );

	for (int i=1; i<=25; ++i ) fprintf_s( resfile, "	%s", GetChromosomeName(i).c_str() );

	fputs( "\n\n", resfile );

	for (long i=0; i<hitmgr.GetGeneListSize(); ++i )
	{
		CHitGene* p_gene = hitmgr.GetGene(i);

		STR gene_name       = p_gene->m_pParent->m_name;
	
		long gene_len       = p_gene->GetGeneLengthAA();  // [aa]
		
		long hits_cnt       = p_gene->GetHitsInChrCount();

		double hits_density = p_gene->GetHitsInChrDensity();

		fprintf_s( resfile, "%s	%ld	%ld	%s", 
			                gene_name.c_str(), 
							gene_len, 
							hits_cnt, 
							GetStr( hits_density, 6 ).c_str() );
	
		for (int i=1; i<=25; ++i )
		{
			long chr_hits = p_gene->GetHitsInChrCount( GetChromosomeName(i) );

			if (use_percent) 
			{
				double k = (hits_cnt==0)? 0.0 : (100.0*(double)chr_hits/(double)hits_cnt);

				fprintf_s( resfile, "	%s", GetStr( k, 1 ).c_str() );
			}
			else
			{
				fprintf_s( resfile, "	%ld", chr_hits );
			}
		}

		fputs( "\n", resfile );
	}

	fclose(resfile);

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

STR GetStr( double val, int prec )
{
	char buf[20];
	sprintf_s( buf, "%1.*f", prec, val );

	STR res = buf;

	size_t pos = res.find( '.' );
	if (pos==std::string::npos) return res;

	res.replace( pos, 1, "," );

	return res;
}


/////////////////////////////////////////////////////////////////////////////////////////

TCHAR* getCmdOption(TCHAR ** begin, TCHAR ** end, const STR& option)
{
    TCHAR ** itr = std::find(begin, end, option);

    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(TCHAR** begin, TCHAR** end, const STR& option)
{
    return std::find(begin, end, option) != end;
}

STR checkPath( STR src )
{
	STR res = src;

	size_t pos = res.rfind("\\");
	if (pos!=(res.length()-1)) res += "\\";
	
	return res;
}
