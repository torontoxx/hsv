#pragma once


struct CChrHits
{
	CChrHits( STR chr );
	~CChrHits();

	void AddHit( long hit_offset, long target_len );

public:

	STR m_chr;

	VECTOR_LONG_LONG m_hit_list; // ���������� ����� � ���������
};


struct CHitGene
{
	CHitGene( CGene* p_parent );
	~CHitGene();

	bool      AddHit( long target_len, STR chr_name, STR chr_strand, long offset_in_gene, long offset_in_chr_p, long offset_in_chr_r );
	CChrHits* FindChrHits( STR chr );
	void      UnitHits();

	long   GetGeneLengthAA() { return m_pParent->m_len; };
	long   GetGeneLengthBP() { return (3*GetGeneLengthAA()); };

	long   GetHitsInGeneCount();         // ����� ����� � ����
	double GetHitsInGeneDensity();       // ����� ����� � ����/����� ����
	long   GetHitsInGeneLength();        // ��������� ����� ���� ����� � ���� [bp]
	double GetHitsInGeneLengthPercent(); // % ����� ����� �� ���� ����� ����

	long   GetHitsInChrCount();          // ����� ����� � ������
	long   GetHitsInChrCount( STR chr ); // ����� ����� � ���������� ���������
	double GetHitsInChrDensity();        // ����� ����� � ������/����� ����

	void   ShowHitsInGene();

private:

	void _UnitHits( VECTOR_LONG_LONG* p_src );
	void _UnitHit( size_t hit_index, PAIR_LONG* p_hit, VECTOR_LONG_LONG* p_src );

public:

	CGene* m_pParent;

	VECTOR_LONG_LONG m_hits_in_gene;  // ���������� ����� � ����

	std::vector<CChrHits*> m_hits_in_chr; // ���������� ����� � ������
};


class CHitsLoader
{
public:
	CHitsLoader(void);
	~CHitsLoader(void);

	void Clear();
	bool Load( STR src_filename, CVirusDataLoader& datamgr, long target_len );

	long      GetGeneListSize()   { return (long)m_gene_list.size(); };
	CHitGene* GetGene( long ind ) { return m_gene_list[ind]; };
	CHitGene* FindGeneByPID( STR pid );
	

private:

	std::vector<CHitGene*> m_gene_list;

};
