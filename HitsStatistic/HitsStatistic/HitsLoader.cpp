#include "StdAfx.h"

#include "Global.h"

#include "VirusDataLoader.h"

#include "HitsLoader.h"


CChrHits::CChrHits( STR chr )
{
	m_chr = chr;
}

CChrHits::~CChrHits()
{
	this->m_hit_list.clear();
}

void CChrHits::AddHit( long hit_offset, long target_len )
{
	this->m_hit_list.push_back(PAIR_LONG( hit_offset, (hit_offset + target_len - 1) ));
}

/////////////////////////////////////////////////////////////////////////////////////////

CHitGene::CHitGene( CGene* p_parent ) 
{ 
	m_pParent = p_parent; 

	for( int i=1; i<=25; ++i ) this->m_hits_in_chr.push_back( new CChrHits(GetChromosomeName(i) ));
}

CHitGene::~CHitGene()
{
	m_pParent = NULL;

	m_hits_in_gene.clear();

	for (size_t i=0; i<m_hits_in_chr.size(); ++i ) delete m_hits_in_chr[i];
	m_hits_in_chr.clear();
}

//---------------------------------------------------------------------------------------

CChrHits* CHitGene::FindChrHits( STR chr )
{
	for (size_t i=0; i<m_hits_in_chr.size(); ++i ) 
	{
		CChrHits* p_chr = m_hits_in_chr[i];

		if (_stricmp( p_chr->m_chr.c_str(), chr.c_str() )==0) return p_chr;
	}
	return NULL;
}

//---------------------------------------------------------------------------------------

bool CHitGene::AddHit( long target_len, STR chr_name, STR chr_strand, long offset_in_gene, long offset_in_chr_p, long offset_in_chr_r )
{
//	if (chr_strand!="+") return true; // ���� �� ������������ ���� � ��������������� ���������

	// �������� ��������� ���������� ���� � ����:

	this->m_hits_in_gene.push_back( PAIR_LONG( offset_in_gene, (offset_in_gene + target_len - 1) ));

	// �������� ��������� ���������� ���� � ���������:

	CChrHits* p_chr = FindChrHits( chr_name );

	if (!p_chr) return ErrMsg( "CHitGene::AddHit", "Can't find chromosome %s", chr_name.c_str() );

	p_chr->AddHit( offset_in_chr_p, target_len );
	
	return true;
}

//---------------------------------------------------------------------------------------

void CHitGene::UnitHits()
{
	_UnitHits( &this->m_hits_in_gene );

	for (size_t i=0; i<m_hits_in_chr.size(); ++i ) 
	{
		CChrHits* p_chr = m_hits_in_chr[i];

		_UnitHits( &p_chr->m_hit_list );
	}
}

//---------------------------------------------------------------------------------------

bool sort_hits_by_pos( PAIR_LONG& p1, PAIR_LONG& p2 )
{
	return (p1.first < p2.first);
}

void CHitGene::_UnitHits( VECTOR_LONG_LONG* p_src )
{
	for (size_t i=0; i<p_src->size(); ++i )
	{
		PAIR_LONG* p_hit = &(*p_src)[i];

		_UnitHit( i, p_hit, p_src );
	}

	std::sort( p_src->begin(), p_src->end(), sort_hits_by_pos );
}

//---------------------------------------------------------------------------------------

void CHitGene::_UnitHit( size_t hit_index, PAIR_LONG* p_hit, VECTOR_LONG_LONG* p_src )
{
	long start = p_hit->first;
	long end   = p_hit->second;

	while(true)
	{
		bool chng = false;

		for (size_t i=(hit_index+1); i<p_src->size(); ++i )
		{
			long curr_start = (*p_src)[i].first;
			long curr_end   = (*p_src)[i].second;

			if (end < curr_start) continue;
			if (start > curr_end) continue;

			start = min( start, curr_start );
			end   = max( end, curr_end );

			p_src->erase( p_src->begin()+i ); // ����������� ��������

			chng = true;
			break;
		}

		if (!chng) break;
	}

	p_hit->first  = start;
	p_hit->second = end;
}

//---------------------------------------------------------------------------------------

long CHitGene::GetHitsInGeneCount()
{
	return (long)m_hits_in_gene.size();
}

//---------------------------------------------------------------------------------------

double CHitGene::GetHitsInGeneDensity()
{
	long gene_len = this->GetGeneLengthAA(); 
	if (gene_len<1) return 0.0;

	return ((double)GetHitsInGeneCount()/(double)gene_len);
}

//---------------------------------------------------------------------------------------

long CHitGene::GetHitsInGeneLength()
{
	long len = 0;

	for (size_t i=0; i<m_hits_in_gene.size(); ++i )
	{
		len += (1 + m_hits_in_gene[i].second - m_hits_in_gene[i].first);
	}
	return len;
}

//---------------------------------------------------------------------------------------

void CHitGene::ShowHitsInGene()
{
	for (size_t i=0; i<m_hits_in_gene.size(); ++i )
	{
		printf_s( "%s:	%ld - %ld\n", this->m_pParent->m_name.c_str(), m_hits_in_gene[i].first, m_hits_in_gene[i].second );
	}
}

//---------------------------------------------------------------------------------------

double CHitGene::GetHitsInGeneLengthPercent()
{
	long gene_len = this->GetGeneLengthBP();
	if (gene_len<1) return 0.0;

	return (100.0*(double)GetHitsInGeneLength()/(double)gene_len);
}

//---------------------------------------------------------------------------------------

long CHitGene::GetHitsInChrCount()
{
	long sum = 0;

	for (size_t i=0; i<m_hits_in_chr.size(); ++i ) sum += (long)m_hits_in_chr[i]->m_hit_list.size();

	return sum;
}

//---------------------------------------------------------------------------------------

long CHitGene::GetHitsInChrCount( STR chr )
{
	CChrHits* p_chr = FindChrHits( chr );

	if (!p_chr) return (long)ErrMsg( "CHitGene::GetHitsInChrCount", "Can't find chromosome %s", chr.c_str() );

	return (long)p_chr->m_hit_list.size();
}

//---------------------------------------------------------------------------------------

double CHitGene::GetHitsInChrDensity()
{
	long gene_len = this->GetGeneLengthAA();  // [aa]
	if (gene_len<1) return 0.0;

	return ((double)GetHitsInChrCount()/(double)gene_len);
}

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////

CHitsLoader::CHitsLoader(void)
{
}

CHitsLoader::~CHitsLoader(void)
{
	void Clear();
}

//---------------------------------------------------------------------------------------

void CHitsLoader::Clear()
{
	for (size_t i=0; i<m_gene_list.size(); ++i ) delete m_gene_list[i];
	m_gene_list.clear();
}

//---------------------------------------------------------------------------------------

bool CHitsLoader::Load( STR src_filename, CVirusDataLoader& datamgr, long target_len )
{
	Clear();
	
	FILE* srcfile = OpenFile( src_filename.c_str() );
	if (!srcfile) return false;

	for (long i=0; i<datamgr.GetGeneListSize(); ++i ) 
	{
		this->m_gene_list.push_back( new CHitGene( datamgr.GetGene(i) ));
	}


	VECTOR_STR str_values;

	long rec_cnt = 0;

	while(!feof(srcfile))
	{
		STR curr_str = SplitFileRecord( srcfile, '\t', &str_values ); 

		if (curr_str=="") break;

		if (str_values.size()!=7) return ErrMsg( "CHitsLoader::Load", "Uncorrect values size in record[%ld]: %ld", rec_cnt+1, str_values.size() );

		STR  curr_chr_name        = str_values[0];
		long curr_chr_len         = atol(str_values[1].c_str());
		STR  curr_chr_strand      = str_values[2];
		STR  curr_pid             = str_values[3];
		long curr_offset_in_gene  = atol(str_values[4].c_str());
		long curr_offset_in_chr_p = atol(str_values[5].c_str());
		long curr_offset_in_chr_r = atol(str_values[6].c_str());

		if (curr_chr_name=="")       return ErrMsg( "CHitsLoader::Load", "Empty chr name in record[%ld]", rec_cnt+1 );
		if (curr_chr_len==0)         return ErrMsg( "CHitsLoader::Load", "Empty chr len in record[%ld]", rec_cnt+1 );
		if (curr_pid=="")            return ErrMsg( "CHitsLoader::Load", "Empty PID in record[%ld]", rec_cnt+1 );
//		if (curr_offset_in_gene==0)  return ErrMsg( "CHitsLoader::Load", "Empty offset in gene in record[%ld]", rec_cnt+1 );
//		if (curr_offset_in_chr_p==0) return ErrMsg( "CHitsLoader::Load", "Empty offset(+) in chr in record[%ld]", rec_cnt+1 );
//		if (curr_offset_in_chr_r==0) return ErrMsg( "CHitsLoader::Load", "Empty offset(-) in chr in record[%ld]", rec_cnt+1 );

		if ((curr_chr_strand!="+") && (curr_chr_strand!="-")) return ErrMsg( "CHitsLoader::Load", "Uncorrect chr strand=%s in record[%ld]", curr_chr_strand.c_str(), rec_cnt+1 );

		CHitGene* p_gene = FindGeneByPID( curr_pid );
		
		if (!p_gene) return ErrMsg( "CHitsLoader::Load", "Can't find gene %s", curr_pid.c_str() );

		if (!p_gene->AddHit( target_len, curr_chr_name, curr_chr_strand, curr_offset_in_gene, curr_offset_in_chr_p, curr_offset_in_chr_r ))
		{
			return ErrMsg( "CHitsLoader::Load", "Can't add hit in record[%ld]", rec_cnt+1 );
		}

		++rec_cnt;
	}

	str_values.clear();

	fclose(srcfile);

	// � ����� ����������� ������������� ��� ���� � ���������� �������������� ���� � ����:

	for (long i=0; i<GetGeneListSize(); ++i )
	{
		CHitGene* p_gene = GetGene(i);
		p_gene->UnitHits();
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

CHitGene* CHitsLoader::FindGeneByPID( STR pid )
{
	for (long i=0; i<GetGeneListSize(); ++i )
	{
		CHitGene* p_gene = GetGene(i);
		if (p_gene->m_pParent->m_PID==pid) return p_gene;
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////////////////
