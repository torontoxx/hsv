#include "StdAfx.h"

#include "Global.h"


//////////////////////////////////////////////////////////////////////////////////////

extern bool ErrMsg( const char* funcname, const char* format, ...)
{
	printf_s( ">>> ERROR %s <<<\n", funcname );
	
	va_list marker;
    va_start( marker, format );
    vprintf(format, marker);
    va_end( marker );

	puts( "!\n" );
	getchar();

    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////

extern FILE* OpenFile( const char* filename )
{
	FILE* pfile = NULL;
	fopen_s( &pfile, filename, "r" );

	if (!pfile) 
	{
		ErrMsg( "OpenFile", "Can't open file %s", filename );
		return NULL;
	}
	return pfile;
}

/////////////////////////////////////////////////////////////////////////////////////////

extern FILE* CreateFile( const char* filename )
{
	FILE* pfile = NULL;
	fopen_s( &pfile, filename, "w" );

	if (!pfile) 
	{
		ErrMsg( "CreateFile", "Can't create file %s", filename );
		return NULL;
	}
	return pfile;
}

/////////////////////////////////////////////////////////////////////////////////////////

extern FILE* AppendFile( const char* filename )
{
	FILE* pfile = NULL;
	fopen_s( &pfile, filename, "a" );

	if (!pfile) 
	{
		ErrMsg( "AppendFile", "Can't open file %s", filename );
		return NULL;
	}
	return pfile;
}

/////////////////////////////////////////////////////////////////////////////////////////

bool FileExist(  const char* filename )
{
	FILE* p_file = NULL;

	fopen_s( &p_file, filename, "r" );

	if (!p_file) return false;

	fclose( p_file );

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

bool SplitString( const STR& src, char delim, VECTOR_STR* p_elems ) 
{
	p_elems->clear();

    std::stringstream ss(src);

    STR item;

    while (std::getline( ss, item, delim )) 
	{
        p_elems->push_back( item );
    }
    return true;
}

//---------------------------------------------------------------------------------------

STR ReadFileRecord( FILE* srcfile )
{
	STR str = "";

	while(!feof(srcfile))
	{
		char ch = fgetc(srcfile);

		if ((ch==EOF) || (ch=='\n')) break;

		str += ch;
	}
	return str;
}

//---------------------------------------------------------------------------------------

STR SplitFileRecord( FILE* srcfile, char delim, VECTOR_STR* p_elems )
{
	STR str = ReadFileRecord( srcfile );

	if (str=="") return str;

	SplitString( str, delim, p_elems );

	return str;
}

//---------------------------------------------------------------------------------------

STR GetChromosomeName( int chr_ind )
{
	if (chr_ind==0)  return "TEST";
	if (chr_ind==23) return "0X";
	if (chr_ind==24) return "0Y";
	if (chr_ind==25) return "MT";

	char buf[10];
	sprintf_s( buf, "%02ld", chr_ind );

	return buf;
}

//////////////////////////////////////////////////////////////////////////////////////

char CGlobal::_getProtein( char a, char b, char c )
{
	char aa = toupper(a);
	char bb = toupper(b);
	char cc = toupper(c);

    if ((aa=='T') && (bb=='T') && (cc=='T')) return 'F';
    if ((aa=='T') && (bb=='T') && (cc=='C')) return 'F';
    if ((aa=='T') && (bb=='T') && (cc=='A')) return 'L';
    if ((aa=='T') && (bb=='T') && (cc=='G')) return 'L';

    if ((aa=='C') && (bb=='T') && (cc=='T')) return 'L';
    if ((aa=='C') && (bb=='T') && (cc=='C')) return 'L';
    if ((aa=='C') && (bb=='T') && (cc=='A')) return 'L';
    if ((aa=='C') && (bb=='T') && (cc=='G')) return 'L';

    if ((aa=='A') && (bb=='T') && (cc=='T')) return 'I';
    if ((aa=='A') && (bb=='T') && (cc=='C')) return 'I';
    if ((aa=='A') && (bb=='T') && (cc=='A')) return 'I';
    if ((aa=='A') && (bb=='T') && (cc=='G')) return 'M';

    if ((aa=='G') && (bb=='T') && (cc=='T')) return 'V';
    if ((aa=='G') && (bb=='T') && (cc=='C')) return 'V';
    if ((aa=='G') && (bb=='T') && (cc=='A')) return 'V';
    if ((aa=='G') && (bb=='T') && (cc=='G')) return 'V';
	//--------------------------------------------------
    if ((aa=='T') && (bb=='C') && (cc=='T')) return 'S';
    if ((aa=='T') && (bb=='C') && (cc=='C')) return 'S';
    if ((aa=='T') && (bb=='C') && (cc=='A')) return 'S';
    if ((aa=='T') && (bb=='C') && (cc=='G')) return 'S';

    if ((aa=='C') && (bb=='C') && (cc=='T')) return 'P';
    if ((aa=='C') && (bb=='C') && (cc=='C')) return 'P';
    if ((aa=='C') && (bb=='C') && (cc=='A')) return 'P';
    if ((aa=='C') && (bb=='C') && (cc=='G')) return 'P';

    if ((aa=='A') && (bb=='C') && (cc=='T')) return 'T';
    if ((aa=='A') && (bb=='C') && (cc=='C')) return 'T';
    if ((aa=='A') && (bb=='C') && (cc=='A')) return 'T';
    if ((aa=='A') && (bb=='C') && (cc=='G')) return 'T';

    if ((aa=='G') && (bb=='C') && (cc=='T')) return 'A';
    if ((aa=='G') && (bb=='C') && (cc=='C')) return 'A';
    if ((aa=='G') && (bb=='C') && (cc=='A')) return 'A';
    if ((aa=='G') && (bb=='C') && (cc=='G')) return 'A';
	//--------------------------------------------------
    if ((aa=='T') && (bb=='A') && (cc=='T')) return 'Y';
    if ((aa=='T') && (bb=='A') && (cc=='C')) return 'Y';
    if ((aa=='T') && (bb=='A') && (cc=='A')) return '*';
    if ((aa=='T') && (bb=='A') && (cc=='G')) return '*';

    if ((aa=='C') && (bb=='A') && (cc=='T')) return 'H';
    if ((aa=='C') && (bb=='A') && (cc=='C')) return 'H';
    if ((aa=='C') && (bb=='A') && (cc=='A')) return 'Q';
    if ((aa=='C') && (bb=='A') && (cc=='G')) return 'Q';

    if ((aa=='A') && (bb=='A') && (cc=='T')) return 'N';
    if ((aa=='A') && (bb=='A') && (cc=='C')) return 'N';
    if ((aa=='A') && (bb=='A') && (cc=='A')) return 'K';
    if ((aa=='A') && (bb=='A') && (cc=='G')) return 'K';

    if ((aa=='G') && (bb=='A') && (cc=='T')) return 'D';
    if ((aa=='G') && (bb=='A') && (cc=='C')) return 'D';
    if ((aa=='G') && (bb=='A') && (cc=='A')) return 'E';
    if ((aa=='G') && (bb=='A') && (cc=='G')) return 'E';
	//--------------------------------------------------
    if ((aa=='T') && (bb=='G') && (cc=='T')) return 'C';
    if ((aa=='T') && (bb=='G') && (cc=='C')) return 'C';
    if ((aa=='T') && (bb=='G') && (cc=='A')) return '*';
    if ((aa=='T') && (bb=='G') && (cc=='G')) return 'W';

    if ((aa=='C') && (bb=='G') && (cc=='T')) return 'R';
    if ((aa=='C') && (bb=='G') && (cc=='C')) return 'R';
    if ((aa=='C') && (bb=='G') && (cc=='A')) return 'R';
    if ((aa=='C') && (bb=='G') && (cc=='G')) return 'R';

    if ((aa=='A') && (bb=='G') && (cc=='T')) return 'S';
    if ((aa=='A') && (bb=='G') && (cc=='C')) return 'S';
    if ((aa=='A') && (bb=='G') && (cc=='A')) return 'R';
    if ((aa=='A') && (bb=='G') && (cc=='G')) return 'R';

    if ((aa=='G') && (bb=='G') && (cc=='T')) return 'G';
    if ((aa=='G') && (bb=='G') && (cc=='C')) return 'G';
    if ((aa=='G') && (bb=='G') && (cc=='A')) return 'G';
    if ((aa=='G') && (bb=='G') && (cc=='G')) return 'G';
	//--------------------------------------------------

	// ����������� ��������� ����������:

	if ((aa=='A') && (bb=='A') && (cc=='R')) return 'K';
	if ((aa=='A') && (bb=='A') && (cc=='Y')) return 'N';
	if ((aa=='A') && (bb=='C') && (cc=='N')) return 'T';
	if ((aa=='A') && (bb=='C') && (cc=='R')) return 'T';

	if ((aa=='A') && (bb=='C') && (cc=='Y')) return 'T';
	if ((aa=='A') && (bb=='G') && (cc=='R')) return 'R';
	if ((aa=='A') && (bb=='G') && (cc=='Y')) return 'S';
	if ((aa=='A') && (bb=='T') && (cc=='Y')) return 'I';
	
	if ((aa=='C') && (bb=='A') && (cc=='R')) return 'Q';
	if ((aa=='C') && (bb=='C') && (cc=='N')) return 'P';
	if ((aa=='C') && (bb=='G') && (cc=='N')) return 'R';
	if ((aa=='C') && (bb=='T') && (cc=='N')) return 'L';
	
	if ((aa=='C') && (bb=='T') && (cc=='Y')) return 'L';
	if ((aa=='G') && (bb=='A') && (cc=='R')) return 'E';
	if ((aa=='G') && (bb=='A') && (cc=='Y')) return 'D';
	if ((aa=='G') && (bb=='C') && (cc=='N')) return 'A';
	
	if ((aa=='G') && (bb=='C') && (cc=='Y')) return 'A';
	if ((aa=='G') && (bb=='G') && (cc=='N')) return 'G';
	if ((aa=='G') && (bb=='G') && (cc=='R')) return 'G';
	if ((aa=='G') && (bb=='G') && (cc=='Y')) return 'G';
	
	if ((aa=='G') && (bb=='T') && (cc=='N')) return 'V';
	if ((aa=='G') && (bb=='T') && (cc=='R')) return 'V';
	if ((aa=='G') && (bb=='T') && (cc=='Y')) return 'V';
	if ((aa=='T') && (bb=='C') && (cc=='N')) return 'S';
	
	if ((aa=='T') && (bb=='C') && (cc=='R')) return 'S';
	if ((aa=='T') && (bb=='C') && (cc=='Y')) return 'S';
	if ((aa=='T') && (bb=='T') && (cc=='R')) return 'L';
	if ((aa=='T') && (bb=='T') && (cc=='Y')) return 'F';
	
	if ((aa=='Y') && (bb=='T') && (cc=='A')) return 'L';
	if ((aa=='Y') && (bb=='T') && (cc=='G')) return 'L';
	//--------------------------------------------------

	return 'X'; 
}

///////////////////////////////////////////////////////////////////////////////////

std::string CGlobal::Translate( size_t offset, std::string& srcdata )
{
	std::string res = "";

	char* resdata = NULL;

	size_t protein_sz = Translate( offset, srcdata.c_str(), srcdata.length(), &resdata );
	if (protein_sz<1) return "";

	res = (std::string)resdata;

	if (resdata) delete[]resdata;

	return res;
}

size_t CGlobal::Translate( size_t offset, const char* srcdata, size_t srcsize, char** resdata )
{
	size_t ressize = 0;

	if ((srcsize<1) || (offset>srcsize)) 
	{
		(*resdata) = new char[1];
		memset( (*resdata), 0, sizeof(char) );
		return 0;
	}

	ressize = (size_t)floor( (double)(srcsize - offset)/3.0 ); //!!!

	(*resdata) = new char[ressize+1];
	memset( (*resdata), 0, sizeof(char)*(ressize+1) );

	size_t protein = 0;

	for ( size_t i=offset; i<srcsize; i+=3, protein++ )
	{
		if (((i+2)>=srcsize) || (protein>=ressize)) {
			break;
		}

		if (protein>=ressize) {
			printf_s("ERROR: ������� ��������� ������ ����������!!!\n");
			return 0;
		}

		(*resdata)[protein] = CGlobal::_getProtein( srcdata[i], srcdata[i+1], srcdata[i+2] );
	}
	
	for ( size_t i=protein; i<ressize; i++ ) (*resdata)[i] = '-';

	return ressize;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

char CGlobal::getReverseNukleotide( char code )
{
	switch (code)
	{
	case 'A':
		return 'T';
	case 'G':
		return 'C';
	case 'T':
		return 'A';
	case 'C':
		return 'G';

	case 'M':
		return 'K';
	case 'R':
		return 'Y';
	case 'W':
		return 'W';
	case 'S':
		return 'S';
	case 'Y':
		return 'R';
	case 'K':
		return 'M';
	case 'V':
		return 'B';
	case 'H':
		return 'D';
	case 'D':
		return 'H';
	case 'B':
		return 'V';


	case 'N':
		return 'N';
	case 'X':
		return 'X';
	case '-':
		return '-';
	}

	return '?';
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool CGlobal::ReverseSequence( std::string* data )
{
	size_t i,j;
	i=0L;
	j=0L;

	if (data->length()==0) return true;

	for (i=(data->length()-1L),j=0L; j<=i; i--,j++)
	{
		char chR = getReverseNukleotide( (*data)[i] );
		char chL = getReverseNukleotide( (*data)[j] );

		(*data)[i] = chL;
		(*data)[j] = chR;

		if (i==0) break;
	}

	return true;
}
