/*
This program was developed as part of work on article 
"Short Nucleotide Sequences in Herpesviral Genomes Identical to the Human DNA", Journal of Theoretical Biology (2015).
Authors: F.Filatov, A.A.Shargunov.
This program converts the raw data for herpesvirus in our own format, more convenient for later use.
For additional information see: https://bitbucket.org/torontoxx/hsv
*/

#include "stdafx.h"

#include "Global.h"

#include "VirusDataLoader.h"

#include "CreateSummaryTable.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//CWinApp theApp;

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		_tprintf(_T("Fatal Error: MFC initialization failed\n"));
		return 1;
	}

	if (argc < 4) 
	{
		puts("Usage: -in <indir> -out <outdir> -t <virus type>"); 
        getchar();
        return 0;
	}

	STR src_virus_folder = getCmdOption(argv, argv + argc, "-in");   
	STR res_virus_folder = getCmdOption(argv, argv + argc, "-out");  
	STR virus_type       = getCmdOption(argv, argv + argc, "-t");

	src_virus_folder = checkPath( src_virus_folder );
	res_virus_folder = checkPath( res_virus_folder );

	_mkdir( res_virus_folder.c_str() );

	VECTOR_STR virus_list;

	virus_list.push_back( virus_type );

	for (size_t i=0; i<virus_list.size(); ++i )
	{
		STR curr_virus_name = virus_list[i];

		printf_s( "\nProcessing %s virus...\n", curr_virus_name.c_str() );

		STR curr_src_folder = src_virus_folder + curr_virus_name + "\\";

		STR curr_res_filename = res_virus_folder + curr_virus_name + ".gff";

		if (ProcessVirus( curr_virus_name, curr_src_folder, curr_res_filename )) printf_s( "OK.\n" );
		else                                                                     printf_s( "FAIL!.\n" );
	}

	printf_s( "\nComplete. Press any key...\n" );
	getchar();

	// clear all:

	virus_list.clear();

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////

bool ProcessVirus( STR virus_name, STR src_folder, STR res_filename )
{
	CVirusDataLoader datamgr;

	FILE* resfile = CreateFile( res_filename.c_str() );
	if (!resfile) return false;

	printf_s( "   loading PTT..." );

	STR src_filename = FindDataFile( "*.ptt", src_folder );
	
	if (src_filename=="") return ErrMsg( "ProcessVirus", "Can't find PTT file in %s", src_folder.c_str());

	datamgr.LoadPTT( src_filename );

	printf_s( "OK (%ld)\n", datamgr.GetGeneListSize() );


	printf_s( "   loading FAA..." );

	src_filename = FindDataFile( "*.faa", src_folder );
	
	if (src_filename=="") return ErrMsg( "ProcessVirus", "Can't find FAA file in %s", src_folder.c_str());

	datamgr.LoadFAA( src_filename );

	printf_s( "OK\n" );


	printf_s( "   loading FFN..." );

	src_filename = FindDataFile( "*.ffn", src_folder );
	
	if (src_filename=="") return ErrMsg( "ProcessVirus", "Can't find FFN file in %s", src_folder.c_str());

	datamgr.LoadFFN( src_filename );

	printf_s( "OK\n" );


	printf_s( "   saving summary data..." );

	for (long i=0; i<datamgr.GetGeneListSize(); ++i )
	{
		CGene* p_gene = datamgr.GetGene(i);

		SaveGene( resfile, p_gene );
	}

	printf_s( "OK\n" );

	fclose(resfile);

	// clear all:

	datamgr.Clear();

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

bool SaveGene( FILE* resfile, CGene* p_gene )
{
	fprintf_s( resfile, "%s	%s	%s	%ld	%ld	", 
		                p_gene->m_name.c_str(), 
						p_gene->m_PID.c_str(), 
						p_gene->m_isstraight?"+":"-", 
						p_gene->m_pos1, 
						p_gene->m_pos2 ); 
	
	fprintf_s( resfile, "%s	%s	%ld	%s	%s", 
		                p_gene->m_synonym.c_str(), 
						p_gene->m_product.c_str(), 
						p_gene->m_len,
						p_gene->m_prot_seq.c_str(), 
						p_gene->m_nukl_seq.c_str() ); 

	fputc( '\n', resfile );

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

STR FindDataFile( STR pattern, STR src_folder )
{
	struct _finddata_t c_file;

	STR search_mask = src_folder + pattern;

	STR res = "";

	long hFile = (long)_findfirst( search_mask.c_str(), &c_file );

	if (hFile!=-1L)
	{
		res = src_folder + c_file.name;

		_findclose( hFile );
	}
	return res;
}


/////////////////////////////////////////////////////////////////////////////////////////

TCHAR* getCmdOption(TCHAR ** begin, TCHAR ** end, const STR& option)
{
    TCHAR ** itr = std::find(begin, end, option);

    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(TCHAR** begin, TCHAR** end, const STR& option)
{
    return std::find(begin, end, option) != end;
}

STR checkPath( STR src )
{
	STR res = src;

	size_t pos = res.rfind("\\");
	if (pos!=(res.length()-1)) res += "\\";
	
	return res;
}
