#pragma once

#include "resource.h"

bool ProcessVirus( STR virus_name, STR src_folder, STR res_filename );

bool SaveGene( FILE* resfile, CGene* p_gene );

STR FindDataFile( STR pattern, STR src_folder );


TCHAR* getCmdOption(TCHAR** begin, TCHAR** end, const STR& option);
bool   cmdOptionExists(TCHAR** begin, TCHAR** end, const STR& option);
STR    checkPath( STR src );
