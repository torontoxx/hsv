#pragma once

struct CGene
{
	CGene( STR pid, STR name, STR synonym, STR product, bool isstraight, long pos1, long pos2, long len );

	bool AppendProteinSeq( STR& seq );

	STR  m_PID;
	STR  m_name;
	STR  m_synonym;
	STR  m_product;
	bool m_isstraight;
	long m_pos1;
	long m_pos2;
	long m_len;
	STR  m_prot_seq;
	STR  m_nukl_seq;
};


class CVirusDataLoader
{
public:
	CVirusDataLoader(void);
	~CVirusDataLoader(void);
	void Clear();

	bool LoadPTT( STR src_filename );
	bool LoadFAA( STR src_filename );
	bool LoadFFN( STR src_filename );

	long   GetGeneListSize()   { return (long)m_gene_list.size(); };
	CGene* GetGene( long ind ) { return m_gene_list[ind];         };
	CGene* FindGeneByPID( STR pid );

private:

	bool _SplitLocation( STR& loc, long* p_pos1, long* p_pos2 );
	bool _IsLocusHeader( STR& src );
	bool _SetGeneNuklSequence( STR& seq );

private:

	std::vector<CGene*> m_gene_list;
};
