#include "StdAfx.h"

#include "Global.h"

#include "VirusDataLoader.h"


CGene::CGene( STR pid, STR name, STR synonym, STR product, bool isstraight, long pos1, long pos2, long len )
{
	m_PID        = pid;
	m_name       = name;
	m_synonym    = synonym;
	m_product    = product;
	m_isstraight = isstraight;
	m_pos1       = pos1;
	m_pos2       = pos2;
	m_len        = len;
	
	m_prot_seq   = "";
	m_nukl_seq   = "";
}

bool CGene::AppendProteinSeq( STR& seq )
{
	for (size_t i=0; i<seq.length(); ++i )
	{
		if (isspace(seq[i])) return ErrMsg( "CGene::AppendProteinSeq", "Unexpected space in protein seq: \"%s\"", seq.c_str() );
	}

	this->m_prot_seq += seq;

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

CVirusDataLoader::CVirusDataLoader(void)
{
}

CVirusDataLoader::~CVirusDataLoader(void)
{
	Clear();
}

//---------------------------------------------------------------------------------------

void CVirusDataLoader::Clear()
{
	for (size_t i=0; i<m_gene_list.size(); ++i ) delete m_gene_list[i];
	m_gene_list.clear();
}

/////////////////////////////////////////////////////////////////////////////////////////

bool sort_genes_by_pos( CGene* g1, CGene* g2 )
{
	if (g1->m_pos1 < g2->m_pos1) return true;
	if (g1->m_pos1 > g2->m_pos1) return false;

	return (g1->m_pos2 < g2->m_pos2);
}

/////////////////////////////////////////////////////////////////////////////////////////

bool CVirusDataLoader::LoadPTT( STR src_filename )
{
	Clear(); //!!!!

	FILE* srcfile = OpenFile( src_filename.c_str() );
	if (!srcfile) return false;

	VECTOR_STR str_values;

	SplitFileRecord( srcfile, '\t', &str_values ); // skip header
	SplitFileRecord( srcfile, '\t', &str_values ); // skip header 
	SplitFileRecord( srcfile, '\t', &str_values ); // skip header

	long rec_cnt = 0;

	while(!feof(srcfile))
	{
		STR curr_str = SplitFileRecord( srcfile, '\t', &str_values ); 

		if (curr_str=="") break;

		if (str_values.size()!=9) return ErrMsg( "LoadPTT", "Uncorrect values size in record[%ld]: %ld", rec_cnt+1, str_values.size() );

		STR curr_loc     = str_values[0];
		STR curr_strand  = str_values[1];
		STR curr_len     = str_values[2];
		STR curr_pid     = str_values[3];
		STR curr_name    = str_values[4];
		STR curr_synonym = str_values[5];
		STR curr_product = str_values[8];

		if (curr_loc=="")     return ErrMsg( "LoadPTT", "Empty location in record[%ld]", rec_cnt+1 );
		if (curr_len=="")     return ErrMsg( "LoadPTT", "Empty length in record[%ld]", rec_cnt+1 );
		if (curr_pid=="")     return ErrMsg( "LoadPTT", "Empty PID in record[%ld]", rec_cnt+1 );
		if (curr_name=="")    return ErrMsg( "LoadPTT", "Empty name in record[%ld]", rec_cnt+1 );
		if (curr_synonym=="") return ErrMsg( "LoadPTT", "Empty synonym in record[%ld]", rec_cnt+1 );
		if (curr_product=="") return ErrMsg( "LoadPTT", "Empty product in record[%ld]", rec_cnt+1 );

		if ((curr_strand!="+") && (curr_strand!="-")) return ErrMsg( "LoadPTT", "Uncorrect strand=%s in record[%ld]", curr_strand.c_str(), rec_cnt+1 );

		long curr_pos1, curr_pos2;

		if (!_SplitLocation( curr_loc, &curr_pos1, &curr_pos2 )) return ErrMsg( "LoadPTT", "Uncorrect location %s in record[%ld]", curr_loc.c_str(), rec_cnt+1 );

		if (this->FindGeneByPID( curr_pid )) return ErrMsg( "LoadPTT", "Gene %s (%s) already exists", curr_pid.c_str(), curr_name.c_str() );

		CGene* p_gene = new CGene( curr_pid, curr_name, curr_synonym, curr_product, (curr_strand=="+"), curr_pos1, curr_pos2, atol(curr_len.c_str()) );

		this->m_gene_list.push_back( p_gene );

		++rec_cnt;
	}

	fclose(srcfile);

	str_values.clear();

	std::sort( m_gene_list.begin(), m_gene_list.end(), sort_genes_by_pos );

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

bool CVirusDataLoader::LoadFAA( STR src_filename )
{
	FILE* srcfile = OpenFile( src_filename.c_str() );
	if (!srcfile) return false;

	VECTOR_STR str_values;

	long rec_cnt = 0;

	CGene* p_curr_gene = NULL;

	while(!feof(srcfile))
	{
		STR curr_str = ReadFileRecord( srcfile );
		if (curr_str=="") break;

		if (_IsLocusHeader( curr_str ))
		{
			SplitString( curr_str, '|', &str_values ); 

			if (str_values.size()<5) return ErrMsg( "LoadFAA", "Uncorrect values size in record[%ld]: %ld", rec_cnt+1, str_values.size() );

			if (str_values[2]!="ref") return ErrMsg( "LoadFAA", "Uncorrect header value %s in record[%ld]", str_values[2].c_str(), rec_cnt+1 );

			STR curr_pid = str_values[1];

			if (curr_pid=="") return ErrMsg( "LoadFAA", "Empty PID in record[%ld]", rec_cnt+1 );

			p_curr_gene = this->FindGeneByPID( curr_pid );

			if (!p_curr_gene) return ErrMsg( "LoadFAA", "Can't find gene with PID %s", curr_pid.c_str() );

			if (p_curr_gene->m_prot_seq!="") return ErrMsg( "LoadFAA", "Gene with PID %s in record[%ld] already has protein", curr_pid.c_str(), rec_cnt+1 );
		}
		else
		{
			if (!p_curr_gene) return ErrMsg( "LoadFAA", "Parent gene is NULL in record[%ld]", rec_cnt+1 );

			p_curr_gene->AppendProteinSeq( curr_str );
		}

		++rec_cnt;
	}

	fclose(srcfile);

	str_values.clear();


	for ( long i=0; i<GetGeneListSize(); ++i )
	{
		CGene* p_gene = GetGene(i);
	
		if (p_gene->m_prot_seq=="") return ErrMsg( "LoadFAA", "Empty protein in gene %s", p_gene->m_PID.c_str() );

		if ((long)p_gene->m_prot_seq.length()!=p_gene->m_len) return ErrMsg( "LoadFAA", "Not equal gene %s protein len", p_gene->m_PID.c_str() );
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

bool CVirusDataLoader::LoadFFN( STR src_filename )
{
	FILE* srcfile = OpenFile( src_filename.c_str() );
	if (!srcfile) return false;

	VECTOR_STR str_values;

	long rec_cnt = 0;

	STR curr_seq = "";

	while(!feof(srcfile))
	{
		STR curr_str = ReadFileRecord( srcfile );
		if (curr_str=="") break;

		if (_IsLocusHeader( curr_str ))
		{
			SplitString( curr_str, '|', &str_values ); 

			if (str_values.size()<5)  return ErrMsg( "LoadFFN", "Uncorrect values size in record[%ld]: %ld", rec_cnt+1, str_values.size() );
			if (str_values[2]!="ref") return ErrMsg( "LoadFFN", "Uncorrect header value %s in record[%ld]", str_values[2].c_str(), rec_cnt+1 );

			if (!_SetGeneNuklSequence( curr_seq )) return ErrMsg( "LoadFFN", "Can't set nukleotide sequence in record[%ld]", rec_cnt+1 );

			curr_seq = "";
		}
		else
		{
			curr_seq += curr_str;
		}

		++rec_cnt;
	}

	// �� �������� ��� ��������� �����:

	if (!_SetGeneNuklSequence( curr_seq )) return ErrMsg( "LoadFFN", "Can't set nukleotide sequence in record[%ld]", rec_cnt+1 );

	fclose(srcfile);

	str_values.clear();

	for ( long i=0; i<GetGeneListSize(); ++i )
	{
		CGene* p_gene = GetGene(i);
		if (p_gene->m_nukl_seq=="") return ErrMsg( "LoadFFN", "Empty sequence in gene %s (%s)", p_gene->m_name.c_str(), p_gene->m_PID.c_str() );
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

bool CVirusDataLoader::_IsLocusHeader( STR& src )
{
	if (src.length()<3) return false;

	STR prefix;
	prefix.assign( src, 0, 3 );

	return (prefix==">gi");
}

/////////////////////////////////////////////////////////////////////////////////////////

bool CVirusDataLoader::_SplitLocation( STR& loc, long* p_pos1, long* p_pos2 )
{
	*p_pos1 = 0;
	*p_pos2 = 0;

	if (loc=="") return false;

	size_t pos = loc.find( ".." );
	if (pos==std::string::npos) return ErrMsg( "_SplitLocation", "Can't find sep in %s", loc.c_str() );

	STR l, r;

	l.assign( loc, 0, pos );

	pos += strlen( ".." );

	r.assign( loc, pos, loc.length()-pos );

	long val1 = atol(l.c_str());
	long val2 = atol(r.c_str());

	if (val1<1) return ErrMsg( "_SplitLocation", "Uncorrect pos1 %ld in %s", val1, loc.c_str() );
	if (val2<1) return ErrMsg( "_SplitLocation", "Uncorrect pos2 %ld in %s", val2, loc.c_str() );

	*p_pos1 = min( val1, val2 );
	*p_pos2 = max( val1, val2 );

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

bool CVirusDataLoader::_SetGeneNuklSequence( STR& seq )
{
	if (seq=="") return true;

	STR prot = CGlobal::Translate( 0, seq );

	if (prot=="") return ErrMsg( "CVirusDataLoader::_SetGeneNuklSequence", "Empty translated protein" );

	if (prot[prot.length()-1]=='*') prot.erase( prot.length()-1, 1 ); // ������� ����-�����

	for ( long i=0; i<GetGeneListSize(); ++i )
	{
		CGene* p_gene = GetGene(i);

		if (p_gene->m_nukl_seq!="") continue; // �.�. ���� ���� � ����������� �������
		
		if (p_gene->m_prot_seq==prot)
		{
			p_gene->m_nukl_seq = seq;  // ����������� ������� ���� ����. ������-��
			return true;
		}
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////

CGene* CVirusDataLoader::FindGeneByPID( STR pid )
{
	for (long i=0; i<GetGeneListSize(); ++i )
	{
		CGene* p_gene = GetGene(i);
		if (p_gene->m_PID==pid) return p_gene;
	}
	return NULL;
}


