#pragma once

#include <math.h>
#include <stdio.h>
#include <io.h>
#include <time.h>
#include <direct.h>

#include <vector>
#include <string>
#include <map>
#include <list>
#include <algorithm>

#include <fstream>
#include <iostream>
#include <locale>
#include <sstream>

typedef std::string                   STR;

typedef std::vector<STR>              VECTOR_STR;
typedef std::vector<long>             VECTOR_LONG;

typedef std::pair<STR,long>           PAIR_STR_LONG;
typedef std::pair<STR,STR>            PAIR_STR;
typedef std::pair<long,long>          PAIR_LONG;
typedef std::pair<double,long>        PAIR_DOUBLE_LONG;
typedef std::pair<long,STR>           PAIR_LONG_STR;

typedef std::vector<PAIR_STR_LONG>    VECTOR_STR_LONG;
typedef std::vector<PAIR_DOUBLE_LONG> VECTOR_DOUBLE_LONG;
typedef std::vector<PAIR_STR>         VECTOR_STR_STR;
typedef std::vector<PAIR_LONG>        VECTOR_LONG_LONG;

typedef std::map<STR,STR>             MAP_STR;
typedef std::map<STR,STR>::iterator   ITERATOR_STR;

typedef std::map<STR,long>            MAP_STR_LONG;
typedef std::map<STR,long>::iterator  ITERATOR_STR_LONG;

typedef std::map<long,STR>            MAP_LONG_STR;
typedef std::map<long,STR>::iterator  MAP_LONG_STR_ITER;


extern bool  ErrMsg( const char* funcname, const char* format, ...);
extern FILE* OpenFile( const char* filename );
extern FILE* CreateFile( const char* filename );
extern bool  FileExist(  const char* filename );

extern bool  SplitString( const STR& src, char delim, VECTOR_STR* p_elems );
extern STR   SplitFileRecord( FILE* srcfile, char delim, VECTOR_STR* p_elems );
extern STR   ReadFileRecord( FILE* srcfile );


class CGlobal
{
	CGlobal();
	~CGlobal();

public:

	static char _getProtein( char a, char b, char c );

	static bool ReverseSequence( std::string* data );
	static char getReverseNukleotide( char code );

	static std::string Translate( size_t offset, std::string& srcdata );
	static size_t      Translate( size_t offset, const char* srcdata, size_t srcsize, char** resdata );
};
