"""
This program was developed as part of work on article 
"Short Nucleotide Sequences in Herpesviral Genomes Identical to the Human DNA", Journal of Theoretical Biology (2015).
Authors: F.Filatov, A.A.Shargunov.
This program converts the raw data for the human DNA (chromosomes) from FASTA into EMBL format for later use.
For additional information see: https://bitbucket.org/torontoxx/hsv
"""

import os, glob
import sys, getopt


def GetSequenceLength( filename ):
    
    row_count = 0
    
    seq_len = 0
    
    with open( filename ) as fin:
        for line in fin:
            
            row_count += 1
            
            if line.startswith(">"):
                if row_count!=1: raise Exception("Unexpected header")
                continue
            
            seq_len += len( line.rstrip() )
            
    return seq_len


########################################################

def ConvertFile( chr_name, seq_len, fin, fout ):
         
    fout.write("ID   chr{0}  PRELIMINARY;   DNA; {1} BP.\n".format( chr_name, seq_len ))
    fout.write("SQ   SEQUENCE {0} BP.\n".format( seq_len ))
     
    bp_cnt, bp_in_col, col_cnt = 0,0,0
     
    for line in fin:
         
        if line.startswith(">"): continue
         
        curr_seq = line.rstrip()
             
        for bp in curr_seq:
         
            if bp_in_col==0 and col_cnt==0: fout.write("     ")
         
            fout.write( bp )
         
            bp_cnt +=1
         
            bp_in_col +=1
         
            if bp_cnt==seq_len:
                fout.write("\n")
             
            elif bp_in_col==10:
                 
                bp_in_col = 0
                 
                col_cnt +=1
             
                if col_cnt==6:
                    col_cnt = 0
                    fout.write( "\n" )
                else:
                    fout.write( " " )
     
    fout.write("//\n")
           

########################################################################################

def GetChrName( full_filename ):
    
    (dirName, fileName) = os.path.split( full_filename )
    
    (fileBaseName, fileExtension) = os.path.splitext( fileName )
    
    index = fileBaseName.rfind("chr")
    
    if index<0: raise Exception("Uncorrect file name")
    
    chr_name = fileBaseName[ index+len("chr") : ]
    
    if len(chr_name)<2: chr_name = "0"+chr_name
    
    return chr_name
    
#######################################################################################

def ProcessFile( full_filename, res_folder ):

    chr_name = GetChrName( full_filename )
    
    if (chr_name==""): raise Exception("Empty chr name")
    
    print( "processing chr", chr_name, end="...")
    
    seq_len = GetSequenceLength( full_filename )
    
    print( seq_len, "bp" )
    
    res_filename = os.path.join( res_folder, "chr"+chr_name+".seq" )
    
    with open( full_filename ) as fin, open( res_filename, "w") as fout:
        
        ConvertFile( chr_name, seq_len, fin, fout )
    

###################################################################

def GetCmdArguments( argv ):
    
    indir, outdir = None,None

    try:
        opts, args = getopt.getopt(argv,"hi:o:",["help","indir=","outdir="])

    except getopt.GetoptError:
        print( 'fasta_to_embl.py -i <inputdir> -o <outputdir>' )
        return (indir,outdir)

    for opt, arg in opts:
        
        if opt == '-h':
            print( 'fasta_to_embl.py -i <inputdir> -o <outputdir>' )
            return (indir,outdir)
    
        elif opt in ("-i", "--indir"):
            indir = arg
        
        elif opt in ("-o", "--outdir"):
            outdir = arg
         
    return (indir,outdir)
         
###################################################################

indir, outdir = GetCmdArguments( sys.argv[1:] )
if not indir or not outdir: sys.exit(2)

src_folder = indir   

res_folder = outdir  
if not os.path.exists(res_folder): os.makedirs(res_folder)

allfiles = glob.glob( src_folder + os.sep + "*chr*.fa" )

print("found {0} files, processing...".format(len(allfiles)))

for filename in allfiles:
    try:
        ProcessFile( filename, res_folder )
    
    except Exception as err:
        print("FAIL in", filename )
        print(err)
        break


print("-"*33)
print("Complete")

