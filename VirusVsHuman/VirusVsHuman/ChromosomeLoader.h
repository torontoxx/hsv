#pragma once


class CChromosomeLoader
{
public:
	CChromosomeLoader();
	~CChromosomeLoader(void);

	void ClearData();

	bool LoadFile( std::string filename, unsigned long min_gap_block, std::vector<unsigned long>* gap_size_list );

	bool GetSequence( unsigned long pos1, unsigned long pos2, char* res );

	char* GetSequence() { return m_pDataBuf; };
	std::string GetSequenceString() { return (std::string)m_pDataBuf; }
	unsigned long GetSequenceSize() { return m_lDataSize; };

	void ReverseSequence();

private:

	unsigned long _ReadHeader( char *data, unsigned long *pos );
	bool          _ReadData( char *srcdata, unsigned long seqsize, char *resdata, unsigned long min_gap_block, std::vector<unsigned long>* gap_size_list );
	bool          _EndOfFile( char chr );
	unsigned long _GetFileSize( std::string filename );

	bool ChrIsNukleotide( char byte )
	{
		char ch = toupper(byte);

		if ((ch=='A') || (ch=='G') || (ch=='C') || (ch=='T') || (ch=='U') || (ch=='N') || (ch=='X')) return true;
		if ((ch=='M') || (ch=='R') || (ch=='W') || (ch=='S') || (ch=='Y') || (ch=='K') || (ch=='V') || (ch=='H') || (ch=='D') || (ch=='B')) return true;

		return false;
	}

	bool ChrIsGap( char byte )
	{
		char ch = toupper(byte);

		return ((ch=='N') || (ch=='X'))? true : false;
	}

private:

	unsigned long m_lDataSize;
	char* m_pDataBuf;
};
