#pragma once

#include "resource.h"

bool LoadChromosome( STR src_chr_filename, CChromosomeLoader* p_chrmgr );

bool ProcessChromosome( bool chr_isstraight, STR chr_name, long target_len, CChromosomeLoader& chrmgr, CVirusDataLoader& datamgr, STR res_filename );

bool ProcessGene( CGene* p_gene, long* p_chr_index, STR chr_name, bool chr_isstraight, const char* p_chr_seq, long chr_len, STR res_filename, long target_len );

bool IsHit( long target_len, long gene_offset, const char* p_gene_seq, long gene_len, long chr_offset, const char* p_chr_seq, long chr_len );

bool SaveHit( long hit_offset_in_chr, long hit_offset_in_gene, CGene* p_gene, STR chr_name, bool chr_isstraight, long chr_len, STR res_filename, long target_len );


bool ConvertSequenceToIndex( const char* p_src, long src_len, long** pp_res );

long GetRaise( long src, long pow );

long GetNuklIndex( char nukl );

long GetWordIndex( const char* p_src, long offset, long src_len );


bool TestIndex( STR src );


TCHAR* getCmdOption(TCHAR** begin, TCHAR** end, const STR& option);
bool   cmdOptionExists(TCHAR** begin, TCHAR** end, const STR& option);
STR    checkPath( STR src );
