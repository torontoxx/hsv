#include "StdAfx.h"

#include "Global.h"

#include "VirusDataLoader.h"


CGene::CGene( STR pid, STR name, STR synonym, STR product, bool isstraight, long pos1, long pos2, long len, STR& prot, STR& nukl )
{
	m_PID        = pid;
	m_name       = name;
	m_synonym    = synonym;
	m_product    = product;
	m_isstraight = isstraight;
	m_pos1       = pos1;
	m_pos2       = pos2;
	m_len        = len;
	m_prot_seq   = prot;
	m_nukl_seq   = nukl;
}

/////////////////////////////////////////////////////////////////////////////////////////

CVirusDataLoader::CVirusDataLoader(void)
{
}

CVirusDataLoader::~CVirusDataLoader(void)
{
	Clear();
}

//---------------------------------------------------------------------------------------

void CVirusDataLoader::Clear()
{
	for (size_t i=0; i<m_gene_list.size(); ++i ) delete m_gene_list[i];
	m_gene_list.clear();
}

/////////////////////////////////////////////////////////////////////////////////////////

bool CVirusDataLoader::Load( STR src_filename )
{
	Clear(); //!!!!

	FILE* srcfile = OpenFile( src_filename.c_str() );
	if (!srcfile) return false;

	VECTOR_STR str_values;

	long rec_cnt = 0;

	while(!feof(srcfile))
	{
		STR curr_str = SplitFileRecord( srcfile, '\t', &str_values ); 

		if (curr_str=="") break;

		if (str_values.size()!=10) return ErrMsg( "Load", "Uncorrect values size in record[%ld]: %ld", rec_cnt+1, str_values.size() );

		STR curr_name    = str_values[0];
		STR curr_pid     = str_values[1];
		STR curr_strand  = str_values[2];
		long curr_pos1   = atol(str_values[3].c_str());
		long curr_pos2   = atol(str_values[4].c_str());
		STR curr_synonym = str_values[5];
		STR curr_product = str_values[6];
		long curr_len    = atol(str_values[7].c_str());
		STR curr_prot    = str_values[8];
		STR curr_nukl    = str_values[9];

		if (curr_name=="")    return ErrMsg( "LoadPTT", "Empty name in record[%ld]", rec_cnt+1 );
		if (curr_pid=="")     return ErrMsg( "LoadPTT", "Empty PID in record[%ld]", rec_cnt+1 );
		if (curr_pos1==0)     return ErrMsg( "LoadPTT", "Empty pos1 in record[%ld]", rec_cnt+1 );
		if (curr_pos2==0)     return ErrMsg( "LoadPTT", "Empty pos2 in record[%ld]", rec_cnt+1 );
		if (curr_synonym=="") return ErrMsg( "LoadPTT", "Empty synonym in record[%ld]", rec_cnt+1 );
		if (curr_product=="") return ErrMsg( "LoadPTT", "Empty product in record[%ld]", rec_cnt+1 );
		if (curr_len==0)      return ErrMsg( "LoadPTT", "Empty length in record[%ld]", rec_cnt+1 );
		if (curr_prot=="")    return ErrMsg( "LoadPTT", "Empty protein in record[%ld]", rec_cnt+1 );
		if (curr_nukl=="")    return ErrMsg( "LoadPTT", "Empty nukleotide in record[%ld]", rec_cnt+1 );

		if ((curr_strand!="+") && (curr_strand!="-")) return ErrMsg( "LoadPTT", "Uncorrect strand=%s in record[%ld]", curr_strand.c_str(), rec_cnt+1 );

		if (this->FindGeneByPID( curr_pid )) return ErrMsg( "Load", "Gene %s (%s) already exists", curr_pid.c_str(), curr_name.c_str() );

		CGene* p_gene = new CGene( curr_pid, 
			                       curr_name, 
								   curr_synonym, 
								   curr_product, 
								   (curr_strand=="+"), 
								   curr_pos1, 
								   curr_pos2, 
								   curr_len,
								   curr_prot, 
								   curr_nukl );

		this->m_gene_list.push_back( p_gene );

		++rec_cnt;
	}

	fclose(srcfile);

	str_values.clear();

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

CGene* CVirusDataLoader::FindGeneByPID( STR pid )
{
	for (long i=0; i<GetGeneListSize(); ++i )
	{
		CGene* p_gene = GetGene(i);
		if (p_gene->m_PID==pid) return p_gene;
	}
	return NULL;
}


