#include "StdAfx.h"

#include "Global.h"

#include "ChromosomeLoader.h"

CChromosomeLoader::CChromosomeLoader()
{
	m_lDataSize = 0;
	m_pDataBuf = NULL;
}

CChromosomeLoader::~CChromosomeLoader(void)
{
	ClearData();
}

void CChromosomeLoader::ClearData(void)
{
	m_lDataSize = 0;

	if (m_pDataBuf) delete []m_pDataBuf;
	m_pDataBuf = NULL;

}

////////////////////////////////////////////////////////////////////////////

bool CChromosomeLoader::LoadFile( std::string filename, unsigned long min_gap_block, std::vector<unsigned long>* gap_size_list )
{
	unsigned long filesz = _GetFileSize(filename);
	if (filesz<1) {
		printf_s("ERROR get file size!\n");
		return false;
	}

	FILE *myfile = NULL;
	fopen_s( &myfile, filename.c_str(), "r");

	if (!myfile) {
		printf_s("ERROR: Can't open the file: %s\n", filename.c_str() );
		return false;
	}

	char *tempbuf = new char[filesz+1];
	memset( tempbuf, 0, sizeof(char)*(filesz+1) );


	if (fread( tempbuf, sizeof(char), filesz, myfile )<=0) 
	{
		printf_s("ERROR fread!\n");
		delete []tempbuf;
		return false;
	}

/*
	unsigned long byte_cnt = 0;
	while(!feof(myfile))
	{
		char ch = fgetc(myfile);
		if (ch==EOF) break;

		if (byte_cnt>filesz) 
		{
			printf_s("FUCK!!!!!!!!!!!!!!!!!!!!!\n");

			break;
		}

		tempbuf[byte_cnt] = ch;

		byte_cnt++;
	}
*/
	if (myfile) fclose(myfile);

	unsigned long pos = 0;
	unsigned long datasize = _ReadHeader( tempbuf, &pos );

	if (datasize>0) 
	{
		ClearData();

		m_lDataSize = datasize;

		m_pDataBuf = new char[m_lDataSize+1];

		memset( m_pDataBuf, 0, sizeof(char)*(m_lDataSize+1) );

		_ReadData( tempbuf+pos, datasize, m_pDataBuf, min_gap_block, gap_size_list );

	}

	if (tempbuf) delete []tempbuf;

	return true;
}

bool CChromosomeLoader::GetSequence( unsigned long pos1, unsigned long pos2, char* res )
{
//	if (pos2>=m_lDataSize) return false;

	unsigned long cnt = 0;
	for (unsigned long i=pos1; i<=pos2; i++,cnt++ ) 
	{
		if (i>=m_lDataSize) res[cnt] = '?';
		else                res[cnt] = m_pDataBuf[i];
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

void CChromosomeLoader::ReverseSequence()
{
	size_t i,j;
	i=0L;
	j=0L;

	if (this->m_lDataSize==0) return;

	for ( i=(this->m_lDataSize-1L), j=0L; j<=i; --i, ++j )
	{
		char chR = CGlobal::getReverseNukleotide( this->m_pDataBuf[i] );
		char chL = CGlobal::getReverseNukleotide( this->m_pDataBuf[j] );

		this->m_pDataBuf[i] = chL;
		this->m_pDataBuf[j] = chR;

		if (i==0) break;
	}
}

///////////////////////////////////////////////////////////////////////////////////


unsigned long CChromosomeLoader::_ReadHeader( char *data, unsigned long *pos  )
{
	*pos = -1;

	unsigned long size1 = -1;
	unsigned long size2 = -1;

	std::string ss = "";

	ss = "";
	char *chr = data;

	int cnt = 0;

	while(cnt<2)
	{
		if (*chr==';') cnt++;
		chr++;
		if (cnt>=2) break;
	}

	ss = "";

	while(!isalpha(*chr)) {
		ss += *chr;
		chr++;
	}

	size1 = atol(ss.c_str());

	while(!isdigit(*chr)) chr++;

	ss = "";
	while(isdigit(*chr)) {
		ss += *chr;
		chr++;
	}

	size2 = atol(ss.c_str());

	while(*chr!='.') chr++;

	*pos = 1 + (unsigned long(chr-data));

	if (size1!=size2) {
		CString ss;
		printf_s("ERROR: Invalid file header: Size1=%lu <> Size2=%lu\n", size1, size2);
		return 0;
	}

	return size1;
}

bool CChromosomeLoader::_ReadData( char *srcdata, unsigned long seqsize, char *resdata, unsigned long min_gap_block, std::vector<unsigned long>* gap_size_list )
{
	// gap statistic:
	bool gap_block_open         = false;
	unsigned long first_gap_pos = 0;
	unsigned long last_gap_pos  = 0;

	unsigned long cnt = 0;

	while(srcdata)
	{
		if (ChrIsNukleotide(*srcdata)) 
		{
			resdata[cnt] = *srcdata;

			if (gap_size_list)
			{
				if (ChrIsGap(*srcdata))
				{
					if (!gap_block_open)
					{
						gap_block_open = true;
						first_gap_pos = cnt;
					}
				}
				else
				{
					if (gap_block_open)
					{
						gap_block_open = false;
						last_gap_pos = cnt;

						unsigned long diff = (last_gap_pos - first_gap_pos);
	
						if (diff>min_gap_block) gap_size_list->push_back( diff );
					
						first_gap_pos = 0;
						last_gap_pos  = 0;
					}
				}
			} // gap

			cnt++;
			if (cnt>=seqsize) break;
		}

		if (_EndOfFile(*srcdata)) break;

		srcdata++;
	}

	if (gap_size_list)
	{
		if (gap_block_open)
		{
			last_gap_pos = cnt;
			unsigned long diff = (last_gap_pos - first_gap_pos);
			if (diff>min_gap_block) gap_size_list->push_back( diff );
		}
	} // gap

	return true;

}

bool CChromosomeLoader::_EndOfFile(char chr)
{
	if ((chr==EOF) || (chr=='/')) return true;

	return false;
}

unsigned long CChromosomeLoader::_GetFileSize( std::string filename )
{
#ifdef _DEBUG
	HANDLE file = CreateFile( (LPCSTR)filename.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );
#else
	HANDLE file = CreateFile( (LPCSTR)filename.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );
#endif

	DWORD size1 = GetFileSize( file, NULL);
	CloseHandle( file);

	return size1;
}