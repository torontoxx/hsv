/*
This program was developed as part of work on article 
"Short Nucleotide Sequences in Herpesviral Genomes Identical to the Human DNA", Journal of Theoretical Biology (2015).
Authors: F.Filatov, A.A.Shargunov.
This program searches strict 20bp homology of herpesvirus genes in the human genome.
For additional information see: https://bitbucket.org/torontoxx/hsv
*/

#include "stdafx.h"

#include "Global.h"

#include "ChromosomeLoader.h"

#include "VirusDataLoader.h"

#include "VirusVsHuman.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define IEOS   -2 
#define IGAP   -1 
#define IBASE   4 
#define ISIZE  15 


//CWinApp theApp;

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		_tprintf(_T("Fatal Error: MFC initialization failed\n"));
		return 1;
	}

	long target_len = 20; // length of homology [bp]

	if (argc < 6) 
	{
		puts("Usage: -vp <src virus dir> -cp <src human genome dir> -vt <virus type> -c1 <first chromosome index> -c2 <last chromosome index>"); 
        getchar();
        return 0;
	}

	STR src_virus_folder = getCmdOption(argv, argv + argc, "-vp");   
	STR src_chr_folder   = getCmdOption(argv, argv + argc, "-cp");   
	STR virus_name       = getCmdOption(argv, argv + argc, "-vt");
	long first_chr_index = atol(getCmdOption(argv, argv + argc, "-c1"));
	long last_chr_index  = atol(getCmdOption(argv, argv + argc, "-c2"));

	src_virus_folder = checkPath( src_virus_folder );
	src_chr_folder   = checkPath( src_chr_folder );

	printf_s( "\n=========== press any key to start... =========\n" );
	getchar();

	//-----------------------------------------------------------------------------------

	STR res_folder = src_virus_folder + "search_result\\";
	_mkdir( res_folder.c_str() );

	char buf[10];
	sprintf_s( buf, "%ld\\", target_len );

	res_folder += buf;

	_mkdir( res_folder.c_str() );

	//-----------------------------------------------------------------------------------

	printf_s( "\nProcessing %s virus...\n", virus_name.c_str() );

	STR res_filename = res_folder + virus_name + ".gff";

	FILE* resfile = CreateFile( res_filename.c_str() );
	if (!resfile) return -1;
	
	fclose(resfile); // ����� ���������, ����� ��������� � ���� ��������� ������

	//--------------------------------------

	STR src_filename = src_virus_folder + virus_name + ".gff";

	printf_s( "   loading PTT..." );

	CVirusDataLoader datamgr;

	datamgr.Load( src_filename );

	printf_s( "OK (%ld)\n", datamgr.GetGeneListSize() );

	//--------------------------------------

	VECTOR_LONG chr_index_list;

	if (first_chr_index<=last_chr_index) 
	{
		for (long i=first_chr_index; i<=last_chr_index; ++i ) chr_index_list.push_back(i);
	}
	else
	{
		for (long i=first_chr_index; i>=last_chr_index; --i ) chr_index_list.push_back(i);
	}

	//--------------------------------------

	for (size_t ind=0; ind<chr_index_list.size(); ++ind )
	{
		long i = chr_index_list[ind];

		char buf[10];
		sprintf_s( buf, "%02ld", i );
		STR curr_chr_name = buf;

		if (i==0)  curr_chr_name = "TEST";
		if (i==23) curr_chr_name = "0X";
		if (i==24) curr_chr_name = "0Y";
		if (i==25) curr_chr_name = "MT";

		printf_s( "\n   Processing chr %s...\n", curr_chr_name.c_str() );

		STR src_chr_filename = src_chr_folder + "chr" + curr_chr_name + ".SEQ";

		CChromosomeLoader chrmgr;

		LoadChromosome( src_chr_filename, &chrmgr ); // ��������� ���������

		ProcessChromosome( true, curr_chr_name, target_len, chrmgr, datamgr, res_filename );

//		LoadChromosome( src_chr_filename, &chrmgr ); // �������������� ���������
//		ProcessChromosome( false, curr_chr_name, target_len, chrmgr, datamgr, res_filename );

		printf_s( "\n   OK(%s)\n", curr_chr_name.c_str() );
	}

	chr_index_list.clear();
	
	//-----------------------------------------------------------------------------------

	printf_s( "\nComplete\n" );
	getchar();

	// clear all:

	datamgr.Clear();

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////

bool LoadChromosome( STR src_chr_filename, CChromosomeLoader* p_chrmgr )
{
	if (p_chrmgr->GetSequenceSize()<1)
	{
		printf_s( "      loading chr..." );

		if (!p_chrmgr->LoadFile( src_chr_filename, 0, 0 ) ) return ErrMsg( "LoadChromosome", "Can't load chromosome" ); 

		printf_s( "OK (%ld)\n", (long)p_chrmgr->GetSequenceSize() );
	}
	else
	{
		printf_s( "\n      reverse chr..." );

		p_chrmgr->ReverseSequence(); // �������������� ��������� !!!!!!

		printf_s( "OK\n\n" );
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

bool ProcessChromosome( bool chr_isstraight, STR chr_name, long target_len, CChromosomeLoader& chrmgr, CVirusDataLoader& datamgr, STR res_filename )
{
	long chr_len = (long)chrmgr.GetSequenceSize();

	printf_s( "      indexing chr..." );

	long* p_chr_index = NULL;

	ConvertSequenceToIndex( chrmgr.GetSequence(), chr_len, &p_chr_index );

	if (!p_chr_index) return ErrMsg( "ProcessChromosome", "Can't convert %s to index", chr_name.c_str() );

	printf_s( "OK\n" );


	for (long g=0; g<datamgr.GetGeneListSize(); ++g )
	{
		CGene* p_gene = datamgr.GetGene(g);

		printf_s( "      %s(%s) %s...", chr_name.c_str(), chr_isstraight? "+":"-", p_gene->m_PID.c_str() );

		ProcessGene( p_gene, p_chr_index, chr_name, chr_isstraight, chrmgr.GetSequence(), chr_len, res_filename, target_len );

		printf_s( "OK\n" );
	}

	// clear all:

	delete []p_chr_index;

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

bool ProcessGene( CGene* p_gene, long* p_chr_index, STR chr_name, bool chr_isstraight, const char* p_chr_seq, long chr_len, STR res_filename, long target_len )
{
	long          gene_len = (long)p_gene->m_nukl_seq.length();
	const char* p_gene_seq = p_gene->m_nukl_seq.c_str();


	long* p_gene_index = NULL;

	ConvertSequenceToIndex( p_gene_seq, gene_len, &p_gene_index );

	if (!p_gene_index) return ErrMsg( "ProcessGene", "Can't convert %s to index", p_gene->m_name.c_str() );


	long* p_curr_chr = p_chr_index;

	long curr_chr_offset = 0;
	
	while( (*p_curr_chr) != IEOS )
	{
		long* p_curr_gene = p_gene_index;

		long curr_gene_offset = 0;

		while( (*p_curr_gene) != IEOS )
		{
			if ((*p_curr_gene)==(*p_curr_chr))
			{
				if (IsHit( target_len, curr_gene_offset, p_gene_seq, gene_len, curr_chr_offset, p_chr_seq, chr_len ))
				{
					SaveHit( curr_chr_offset, curr_gene_offset, p_gene, chr_name, chr_isstraight, chr_len, res_filename, target_len );
				}
			}

			++p_curr_gene;

			++curr_gene_offset;
		}

		++p_curr_chr;

		++curr_chr_offset;
	}

	// clear all:

	delete []p_gene_index;

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

bool IsHit( long target_len, long gene_offset, const char* p_gene_seq, long gene_len, long chr_offset, const char* p_chr_seq, long chr_len )
{
	long g_off = gene_offset + ISIZE;
	long c_off = chr_offset  + ISIZE;

	long last_len = (target_len - ISIZE);

	if ((g_off+last_len) > gene_len) return false;
	if ((c_off+last_len) > chr_len)  return false;

	for (long i=0; i<last_len; ++i )
	{
		if (p_gene_seq[ g_off+i ] != p_chr_seq[ c_off+i ]) return false;
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

bool SaveHit( long hit_offset_in_chr, long hit_offset_in_gene, CGene* p_gene, STR chr_name, bool chr_isstraight, long chr_len, STR res_filename, long target_len )
{
	long chr_pos_p = (chr_isstraight)? hit_offset_in_chr : (chr_len - hit_offset_in_chr - target_len);
	long chr_pos_r = (chr_isstraight)? (chr_len - hit_offset_in_chr - target_len) : hit_offset_in_chr;

	FILE* resfile = AppendFile( res_filename.c_str() );
	if (!resfile) return false;

	fprintf_s( resfile, "%s	%ld	%s	%s	%ld	%ld	%ld\n", 
		                chr_name.c_str(),
						chr_len,
						chr_isstraight? "+":"-",
						p_gene->m_PID.c_str(),
						hit_offset_in_gene,
						chr_pos_p,
						chr_pos_r );

	fclose(resfile);

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
/*
long Search( long target_len, const char* p_gene_seq, long gene_len, const char* p_chr_seq, long chr_len )
{
	char gap_symbol = 'N';

	long found_cnt = 0;

	for (long g=0; g<gene_len; ++g )
	{
		for (long c=0; c<chr_len; ++c )
		{
			bool find_hit = true;

			for (long i=0; i<target_len; ++i )
			{
				if (p_gene_seq[g+i] != p_chr_seq[c+i])
				{
					find_hit = false;
					break;
				}
				if (p_gene_seq[g+i] == gap_symbol)
				{
					find_hit = false;
					break;
				}
			}

			if (find_hit) ++found_cnt;
		}
	}

	return found_cnt;
}
*/
/////////////////////////////////////////////////////////////////////////////////////////

bool ConvertSequenceToIndex( const char* p_src, long src_len, long** pp_res )
{
	if (src_len<1) return ErrMsg( "ConvertSequenceToIndex", "Empty source" );

	*pp_res = new long[src_len+1];
	
	memset( *pp_res, IEOS, sizeof(long)*(src_len+1) ); 

	for (long i=0; i<src_len; ++i )
	{
		(*pp_res)[i] = GetWordIndex( p_src, i, src_len );
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////

long GetWordIndex( const char* p_src, long offset, long src_len )
{
	if ((offset+ISIZE) > src_len) return IEOS; // ����� ������

	long res = 0;

	for ( long i=0; i<ISIZE; ++i )
	{
		long val = GetNuklIndex( p_src[ i+offset ] );
		
		if (val<0) return IGAP; // ����� �������� ���

		res += (val * GetRaise( IBASE, i ));
	}

	return res;
}

/////////////////////////////////////////////////////////////////////////////////////////

long GetNuklIndex( char nukl )
{
	// !!! ������������ �������� ������� ������ ���� ������ ��������� IBASE !!!

	switch (toupper(nukl))
	{
	case 'A':
		return 0;
	case 'G':
		return 1;
	case 'C':
		return 2;
	case 'T':
		return 3;
	}
	return IGAP;
}

/////////////////////////////////////////////////////////////////////////////////////////

long GetRaise( long src, long pow )
{
	if (src==0) return 0;

	long res = 1;

	for (long i=0; i<pow; ++i ) res *= src;

	return res;
}


/////////////////////////////////////////////////////////////////////////////////////////

bool TestIndex( STR src )
{
	long* p_index = NULL;

	ConvertSequenceToIndex( src.c_str(), (long)src.length(), &p_index );

	if (!p_index) return ErrMsg( "Can't convert %s", src.c_str() );

	printf_s( "%s:   ", src.c_str() );
	
	long* p_curr = p_index;

	while( (*p_curr) != IEOS )
	{
		printf_s( "%ld ", (*p_curr) );

		++p_curr;
	}

	printf_s( "IEOS\n" );

	delete []p_index;

	return true;
}


/////////////////////////////////////////////////////////////////////////////////////////

TCHAR* getCmdOption(TCHAR ** begin, TCHAR ** end, const STR& option)
{
    TCHAR ** itr = std::find(begin, end, option);

    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(TCHAR** begin, TCHAR** end, const STR& option)
{
    return std::find(begin, end, option) != end;
}

STR checkPath( STR src )
{
	STR res = src;

	size_t pos = res.rfind("\\");
	if (pos!=(res.length()-1)) res += "\\";
	
	return res;
}
